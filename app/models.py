from datetime import datetime

from flask_login import UserMixin

from app import db, login

from werkzeug.security import generate_password_hash, check_password_hash


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Xssdata(db.Model):
    id = db.Column(db.Integer, unique=True, primary_key=True)
    browser = db.Column(db.String(1000))
    ua = db.Column(db.String(1000))
    os = db.Column(db.String(1000))
    lang = db.Column(db.String(1000))
    referrer = db.Column(db.String(1000))
    location = db.Column(db.String(1000))
    toplocation = db.Column(db.String(1000))
    cookie = db.Column(db.String(1000))
    domain = db.Column(db.String(1000))
    title = db.Column(db.String(1000))
    cpucores = db.Column(db.String(1000))
    body = db.Column(db.String(1000))
    sessionStorage = db.Column(db.String(1000))
    localStorage = db.Column(db.String(1000))
    serverHeaders = db.Column(db.String(1000))
    screen = db.Column(db.String(1000))
    flash = db.Column(db.String(1000))
    loginData = db.Column(db.String(1000))

    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Xssdata {}>'.format(self.id)
